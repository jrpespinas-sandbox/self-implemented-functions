def dataset_split(X, y, train, test, val, seed):
    """
    Self-implemented Train-Test-Val split

    Arguments:
        X: the features.
        y: the labels.
        train: integer 0-100 which determines length of the training set.
        test: integer 0-100 which determines length of the test set.
        val: integer 0-100 which determines length of the validation set.
        seed: value that specifies the deterministic shuffle

    Returns:
        Output the split dataset
    """
    data = np.concatenate((X, y), axis=1)
    np.random.seed(seed)
    np.random.shuffle(data)

    X = data[:, :-1]
    y = data[:, -1]

    length = dataset.shape[0]
    train = train / 100
    test = train + (test / 100)
    val = test + (val / 100)

    train_size = int(round(length * train))
    test_size = int(round(length * test))
    val_size = int(round(length * val))

    train_input = X[0:train_size]
    test_input = X[train_size:test_size]
    val_input = X[test_size:val_size]

    train_label = np.array([y[0:train_size]]).T
    test_label = np.array([y[train_size:test_size]]).T
    val_label = np.array([y[test_size:val_size]]).T

    return train_input, train_label, test_input, test_label, val_input, val_label

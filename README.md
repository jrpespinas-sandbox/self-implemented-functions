# Self-implemented functions in Python

These functions may be used as an alternative to predefined functions from TensorFlow, PyTorch, and Scikit-Learn.
More functions will be added in the future

## Directory

| | **Filename** | **Type** |
| ----- | ------ | ------ |
| **1** | dataset_split.py | data preprocessing |
| **2** | data_cleaning.py | data preprocessing |


## Functions
1.  `dataset_split()` - splits the dataset into, **training set**, **test set**, and **validation set**
2.  (will contain script in a function soon)

## Index
`dataset_split()`,  **1**
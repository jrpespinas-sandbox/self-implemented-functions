import numpy as np
import pandas as pd
import string
import re
import glob

# retrieve all the filepaths of the csv files
filepaths = glob.glob("./*/*.csv")
print(filepaths)

# read file for stopwords
stopwords = pd.read_csv('stopwords.txt', sep=" ", header=None)
stopwords = stopwords.iloc[:,-1].values

for file in filepaths:

    # as long as the name follows: DIRECTORYNAME_dataset/BrandName_sentiments.csv
    # retrieve file name
    filename = file.split("/")[2].split("_")[0]

    '''
    The following are the steps for cleaning the raw dataset:

        1. Remove unnecessary columns
        2. Convert to string
        3. Convert to lowercase
        4. Remove Links/URLs
        5. Remove Hashtags
        6. Remove Handlers
        7. Remove punctuations and emojis
        8. Remove excess spaces
        9. Remove non-utf-8 characters
        10. Remove other special characters
        11. Remove stopwords
        12. Remove exact duplicate rows
        13. Remove blank rows
    '''
    raw = pd.read_csv(file)

    # remove unnecessary columns
    raw = raw.drop(['Unnamed: 0', 'created_time', 'from', 'id'], axis = 1)

    # convert the 'message' column to string
    raw['message'] = [str(x) for x in raw['message']]

    # convert to lowercase and create a new column
    raw['sentiments'] = raw['message'].str.lower()

    # remove links
    raw['sentiments'] = raw['sentiments'].str.replace(r'http\S+', '')

    # remove hashtags
    raw['sentiments'] = raw['sentiments'].str.replace('#\S+', '')

    # remove handlers
    raw['sentiments'] = raw['sentiments'].str.replace(r'@\S+', '')

    # remove every endlines '\n'
    raw['sentiments'] =  raw['sentiments'].str.replace('\\n', ' ')

    # remove extra spaces
    raw['sentiments'] = raw['sentiments'].str.replace(r' +', ' ')

    # removing numbers
    raw['sentiments'] = raw['sentiments'].str.replace(r'[0-9]', '')

    # removes punctuations and emojis
    raw['sentiments'] = raw['sentiments'].str.replace('[^\w\s]', '')

    # remove non-utf-8 characters
    raw['sentiments'] = [bytes(x, 'utf-8').decode('utf-8', 'ignore') for x in raw['sentiments']]

    # remove every special characters
    raw['sentiments'] = [" ".join([re.sub(r'[^A-Za-z0-9]+', '', x) for x in i.split()]) for i in raw['sentiments']]

    # remove Tagalog stopwords (questionable)
    raw['no_stopwords'] = [" ".join([x for x in string.split() if x not in stopwords]) for string in raw['sentiments']]

    # remove duplicated rows (includes empty rows)
    raw.drop_duplicates(subset = "sentiments", keep = False, inplace = True)

    raw.to_csv(filename + "_cleaned_dataset.csv")
